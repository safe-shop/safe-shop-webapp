var mongoose = require('mongoose');

var users = mongoose.Schema({
    name : String,
    phone : Number,
    email : String,
    user_id : Number,
    password : String
},
{
	collection:"Users"
});

module.exports = mongoose.model('Users',users);
