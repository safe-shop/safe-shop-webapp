(function(){

    var dummyHandler = require("./handlers/dummyHandler");
    
	module.exports = function(app){

        app.all("/dummy",dummyHandler.handleView); // For GET and POST Request
		// app.get("/logout",userHandler.logout); // For GET Request
        // app.post("/",dummyHandler.handle);// For POST

        app.post("/api/dummy",dummyHandler.handleApi)
        
	
	};


})();
