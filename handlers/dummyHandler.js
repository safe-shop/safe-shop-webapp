(function(){

const API = 1;
const VIEW = 0;

var dummies = require("../models/dummy");

function handle(input,res,reqtype){

    dummies.findOne({"name" : input.name},function(err,data){
        var output = {"age" : -1 };
        if(data && !err)
        {
            output.age = data.age;
        }
        if(reqtype == API)
            res.json(output);
        else
            res.render("dummy.html",output);
    });

}

module.exports.handleApi = function(req,res){
    
// The API calls are called from Android app. They need the data in JSON format. Hence we use res.json

    var name = req.body.name;
    var input = {
        "name" : name
    };
    handle(input,res,API);
    
}

module.exports.handleView = function(req,res){

    var name = "";
    if(req.method == "POST")
        name = req.body.name;
    else if(req.method == "GET")
        name = req.query.name
    var input = {
        "name" : name    
    }
    handle(input,res,VIEW);
}




})();